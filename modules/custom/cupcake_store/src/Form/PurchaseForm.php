<?php

namespace Drupal\cupcake_store\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\PrivateTempStoreFactory;

/**
 * Class PurchaseForm.
 */
class PurchaseForm extends FormBase {

  /**
   * Drupal\user\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStore;
  /**
   * Constructs a new ParaBorrarForm object.
   */
  public function __construct( PrivateTempStoreFactory $temp_store_factory ) {
    $this->tempStore = $temp_store_factory->get('cupcake_store');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'purchase_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cupcake_nid = NULL) {
    
    $cupcakeNode = \Drupal\node\Entity\Node::load($cupcake_nid);
    $cupcake_name = "Unknown";
    if( !is_null($cupcakeNode)  ) 
      $cupcake_name = $cupcakeNode->getTitle();

    $orderPreview = "";
    $tempPurchases = $this->tempStore->get('purchase');
    
    if( is_array($tempPurchases) ) {
      foreach ($tempPurchases as $key => $value) {
        $tempNode = \Drupal\node\Entity\Node::load($value["cupcake_nid"]);
        if( !is_null($tempNode)  ) {
          $orderPreview .= "<br>".$tempNode->getTitle()." Q. ".$value["quantity"];
        }
      }
    }

    $form['order_preview'] = [
      '#type' => 'item',
      '#title' => $this->t('Resume'),
      '#markup' => $orderPreview,
    ];
    $form['cupcake_nid'] = [
      '#type' => 'hidden',
      '#value' => $cupcake_nid,
    ];
    $form['cupacake_name'] = [
      '#type' => 'item',
      '#title' => t('Cupcake'),
      '#markup' => t( $cupcake_name ),
    ];
    $form['quantity'] = [
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#description' => $this->t('Cupcakes quantity'),
      '#min' => 0,
      '#max' => 100,
      '#default_value' => 1,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Buy'),
    ];
    $form['cancel_order'] = array(
      '#type' => 'button',
      '#value' => t('Cancel order'),
      '#submit' => array('cancelOrderForm'),
    );
    $form['confirm_order'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm order'),
      #'#submit' => array('confirmOrderForm'),
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $tempPurchases = $this->tempStore->get('purchase');
    $tempPurchases = is_array($tempPurchases)?$tempPurchases:array();
    $tempPurchases[] = array( 'cupcake_nid' => $form_state->getValue('cupcake_nid'), 
                              'quantity' => $form_state->getValue('quantity'),
                            );
    $this->tempStore->set('purchase', $tempPurchases);

    drupal_set_message( $form_state->getValue('submit') );
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrderForm(array &$form, FormStateInterface $form_state) {

    $this->tempStore->set('purchase', array());

    drupal_set_message( t("Cancel order successfully") );
  }

}